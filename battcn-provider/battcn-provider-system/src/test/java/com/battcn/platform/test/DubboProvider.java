package com.battcn.platform.test;


/**
 * 
 * @描述: 启动Dubbo服务用的MainClass.
 * @作者: 唐亚峰 .
 */
public class DubboProvider {
	
	public static void main(String[] args) {
		com.alibaba.dubbo.container.Main.main(args);
	}
    
}