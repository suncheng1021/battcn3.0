package com.battcn.platform.system.mapper;

import com.battcn.platform.system.model.LogsEntity;

import tk.mybatis.mapper.common.Mapper;

public interface LogsMapper extends Mapper<LogsEntity>
{

}
